package definitions;

import java.util.HashMap;

public class Context {

    public static HashMap<String, HashMap<String,String>> configs = new HashMap<String, HashMap<String, String>>() {

        {
            HashMap<String, String> posts = new HashMap<String, String>() {

                {
                    put("CONST_APPEND_PATH", "/posts");
                    put("CONST_DEMO_BODY", "{\"userId\":1, \"title\": \"demo\", \"body\": \"demo\"}");
                    put("CONST_JSON_VALIDATOR", "post.json");

                }
            };

            HashMap<String, String> comments = new HashMap<String, String>() {

                {
                    put("CONST_APPEND_PATH", "/comments");
                    put("CONST_DEMO_BODY", "{\"postId\" : 1,\"name\": \"demo\", \"email\": \"a@a.com\", \"body\":\"demo\"}");
                    put("CONST_JSON_VALIDATOR", "comment.json");

                }
            };

            HashMap<String, String> users = new HashMap<String, String>() {

                {
                    put("CONST_APPEND_PATH", "/users");
                    put("CONST_DEMO_BODY", "{}");
                    put("CONST_JSON_VALIDATOR", "users.json");

                }
            };

            put("post", posts);
            put("comment", comments);
            put("user", users);

        }

    };
}
