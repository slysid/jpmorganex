package definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import io.restassured.response.Response;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;


public class Steps {

    private Response response;
    private String param_key;
    private String param_value;
    private String post_body = "{}";

    private final String BASE_URL = "https://jsonplaceholder.typicode.com";
    private final String HEADER_CONTENT_TYPE = "application/json; charset=utf-8";
    private final Long MAX_RESPONSE_TIME = 1500L;
    private final Integer STATUS_CREATED = 201;
    private final Integer STATUS_OK = 200;
    private final Integer CONST_ZERO = 0;
    private final String PATH_USERS="/users";
    private final String[] API_TYPES = {"post", "comment", "user"};

    private  void validate_publish_type(String publish_type) {

       if (ArrayUtils.contains(API_TYPES, publish_type) == false) {

           System.out.println(publish_type + " not a valid api");
           throw new NullPointerException();
       }
    }

    private void submit_get_request(String get_url) {

        this.response = given().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                when().get(get_url);
    }


    @Given("a user data {string}")
    public void a_user_data(String user_data) {

        this.post_body = user_data;
    }


    @When("users publish a {string}")
    public void users_publish_a(String api_type) {

        this.validate_publish_type(api_type);
        String url_append_path = Context.configs.get(api_type).get("CONST_APPEND_PATH");

        String publish_url = this.BASE_URL + url_append_path;
        this.response = given().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(this.post_body).
                when().
                post(publish_url);
    }

    @Then("server should handle and return response code {int}")
    public void server_should_handle_and_return_response_code(Integer status_code) {

        this.response.then().statusCode(status_code);
    }


    @Given("a successful publish response for a user {string}")
    public void a_successful_publish_response(String api_type) {

        this.validate_publish_type(api_type);
        String post_body = Context.configs.get(api_type).get("CONST_DEMO_BODY");

        this.a_user_data(post_body);
        this.users_publish_a(api_type);
        this.server_should_handle_and_return_response_code(this.STATUS_CREATED);
    }

    @Then("response should have valid content type header")
    public void response_should_have_valid_content_type_header() {

        String content_type = this.response.getContentType();
        Assert.assertNotNull(content_type);
        Assert.assertEquals(content_type, this.HEADER_CONTENT_TYPE);
    }


    @Then("response should have a valid JSON schema for a {string} response")
    public void response_should_have_a_valid_JSON_schema_for_response(String api_type) {

        this.validate_publish_type(api_type);
        String validator = Context.configs.get(api_type).get("CONST_JSON_VALIDATOR");
        this.response.then().assertThat().body(matchesJsonSchemaInClasspath(validator));
    }


    @Then("validate the response time from server")
    public void validate_the_response_time_from_server() {

         this.response.then().time(lessThan(this.MAX_RESPONSE_TIME));
    }

    @Given("a user want to retrieve user details for all users")
    public void a_user_want_to_retrieve_user_details_for_all_users() {

        this.submit_get_request(this.BASE_URL + this.PATH_USERS);
    }


    @Given("a user want to retrieve user details by {string}")
    public void a_user_want_to_retrieve_user_details_by(String param_key) {

        this.param_key = param_key;
    }

    @When("submit a request with value {string}")
    public void submit_a_request_with_value(String param_value) {

        this.param_value = param_value;
        String submit_url = this.BASE_URL + this.PATH_USERS + "?" + this.param_key + "=" + this.param_value;
        this.submit_get_request(submit_url);

    }

    @Then("server should handle and return requested details")
    public void server_should_handle_and_return_requested_details() {

        this.server_should_handle_and_return_response_code(this.STATUS_OK);
        String actual = this.response.jsonPath().getString(this.param_key+"[0]");
        Assert.assertEquals(this.param_value,actual);
    }

    @Then("server should handle and return no such details")
    public void server_should_handle_and_return_no_such_details() {

        this.server_should_handle_and_return_response_code(this.STATUS_OK);
        this.response.then().body("results", hasSize(equalTo(this.CONST_ZERO)));
    }

}
