Feature: List users
  User test scenarios to retrieve user details

  @regression @smoke
  Scenario: Get All Users
    Given a user want to retrieve user details for all users
    Then server should handle and return response code 200

  @regression @smoke
  Scenario: Get A User by id
    Given a user want to retrieve user details by "id"
    When submit a request with value "1"
    Then server should handle and return requested details

  @regression @smoke
  Scenario: Get A User by name
    Given a user want to retrieve user details by "name"
    When submit a request with value "Leanne Graham"
    Then server should handle and return requested details

  @regression
  Scenario: Get A User by case sensitive name
    Given a user want to retrieve user details by "name"
    When submit a request with value "leanne graham"
    Then server should handle and return no such details

  @regression
  Scenario: Get A User by non existent name
    Given a user want to retrieve user details by "name"
    When submit a request with value "JP2019"
    Then server should handle and return no such details

  @regression @smoke
  Scenario: Get A User by username
    Given a user want to retrieve user details by "username"
    When submit a request with value "Bret"
    Then server should handle and return requested details

  @regression
  Scenario: Get A User by case sensitive username
    Given a user want to retrieve user details by "username"
    When submit a request with value "bret"
    Then server should handle and return no such details

  @regression
  Scenario: Get A User by non existent username
    Given a user want to retrieve user details by "username"
    When submit a request with value "JP2019"
    Then server should handle and return no such details

  @regression @smoke
  Scenario: Get A User by email
    Given a user want to retrieve user details by "email"
    When submit a request with value "Sincere@april.biz"
    Then server should handle and return requested details


  @regression
  Scenario: Get A User by invalid email
    Given a user want to retrieve user details by "email"
    When submit a request with value "Sincere"
    Then server should handle and return no such details


  @regression
  Scenario: Get A User by non existent email
    Given a user want to retrieve user details by "email"
    When submit a request with value "Sincere@april.bi"
    Then server should handle and return no such details

  @regression @smoke
  Scenario: Validate Response For Valid Content-Type Header
    Given a user want to retrieve user details for all users
    Then response should have valid content type header

  @regression @smoke
  Scenario: Validate Response For Valid JSON Schema
    Given a user want to retrieve user details for all users
    Then response should have a valid JSON schema for a "user" response

  @regression @smoke
  Scenario: Validate The Response Time To Publish a Post
    Given a user want to retrieve user details for all users
    Then validate the response time from server

