Feature: Publish Posts
  Post publish test scenarios.Retrieval scenarios are excluded.

  @regression @smoke
  Scenario: Publish a Post With Valid Details
    Given a user data "{\"userId\" : 1, \"title\": \"demo\", \"body\": \"demo\"}"
    When users publish a "post"
    Then server should handle and return response code 201

  @regression
  Scenario: Publish a Post With Invalid JSON Body
    Given a user data "{\"userId\" : 1, \"title\": \"demo\", \"body\": \"demo\",}"
    When users publish a "post"
    Then server should handle and return response code 500

  @regression
  Scenario: Publish a Post With Missing UserId
    Given a user data "{\"title\": \"demo\", \"body\": \"demo\"}"
    When users publish a "post"
    Then server should handle and return response code 201

  @regression
  Scenario: Publish a Post With Missing Title
    Given a user data "{\"userId\" : 1, \"body\": \"demo\"}"
    When users publish a "post"
    Then server should handle and return response code 201

  @regression
  Scenario: Publish a Post With Missing Body
    Given a user data "{\"userId\" : 1, \"title\": \"demo\"}"
    When users publish a "post"
    Then server should handle and return response code 201

  @regression @smoke
  Scenario: Validate a Post Publish Response For Valid Content-Type Header
    Given a successful publish response for a user "post"
    Then response should have valid content type header

  @regression @smoke
  Scenario: Validate a Post Publish Response For Valid JSON Schema
    Given a successful publish response for a user "post"
    Then response should have a valid JSON schema for a "post" response


  @regression @smoke
  Scenario: Validate The Response Time To Publish a Post
    Given a successful publish response for a user "post"
    Then validate the response time from server
