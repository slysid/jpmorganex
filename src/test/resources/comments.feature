Feature: Publish Comments
  Comment publish test scenarios.Retrieval scenarios are excluded.

  @regression @smoke
  Scenario: Publish a Comment With Valid Details
    Given a user data "{\"postId\" : 1, \"name\": \"demo\", \"email\": \"a@a.com\", \"body\":\"demo\"}"
    When users publish a "comment"
    Then server should handle and return response code 201

  @regression
  Scenario: Publish a Comment With Invalid JSON Body
    Given a user data "{\"postId\" : 1, \"name\": \"demo\", \"email\": \"a@a.com\", \"body\":\"demo\",}"
    When users publish a "post"
    Then server should handle and return response code 500

  @regression
  Scenario: Publish a Comment With Missing postId
    Given a user data "{\"name\": \"demo\", \"email\": \"a@a.com\", \"body\":\"demo\"}"
    When users publish a "comment"
    Then server should handle and return response code 201

  @regression
  Scenario: Publish a Comment With Missing name
    Given a user data "{\"postId\" : 1, \"email\": \"a@a.com\", \"body\":\"demo\"}"
    When users publish a "comment"
    Then server should handle and return response code 201

  @regression
  Scenario: Publish a Comment With Missing email
    Given a user data "{\"postId\" : 1, \"name\": \"demo\", \"body\":\"demo\"}"
    When users publish a "comment"
    Then server should handle and return response code 201

  @regression
  Scenario: Publish a Comment With Invalid email
    Given a user data "{\"postId\" : 1, \"name\": \"demo\", \"email\": \"a\", \"body\":\"demo\"}"
    When users publish a "comment"
    Then server should handle and return response code 201

  @regression
  Scenario: Publish a Comment With Missing body
    Given a user data "{\"postId\" : 1, \"name\": \"demo\", \"email\": \"a@a.com\"}"
    When users publish a "comment"
    Then server should handle and return response code 201

  @regression @smoke
  Scenario: Validate a Comment Publish Response For Valid Content-Type Header
    Given a successful publish response for a user "comment"
    Then response should have valid content type header

  @regression @smoke
  Scenario: Validate a Comment Publish Response For Valid JSON Schema
    Given a successful publish response for a user "comment"
    Then response should have a valid JSON schema for a "comment" response


  @regression @smoke
  Scenario: Validate The Response Time To Publish a Comment
    Given a successful publish response for a user "comment"
    Then validate the response time from server

